<?php

namespace HeinHanekom\ChuckNorrisJokes\Tests;

use HeinHanekom\ChuckNorrisJokes\JokeFactory;
use PHPUnit\Framework\TestCase;

class JokeFactoryTest extends TestCase
{
    /** @test */
    public function it_returns_a_predefined_joke()
    {
        $chuckNorrisJokes = [
            'Joke 1',
            'Joke 2',
            'Joke 3',
            'Joke 4',
        ];

        $jokes = new JokeFactory($chuckNorrisJokes);

        $joke = $jokes->getRandomJoke();

        $this->assertContains($joke, $chuckNorrisJokes);

    }

    /** @test */
    public function it_returns_a_default_joke()
    {
        $jokes = new JokeFactory([
            'This is a new joke',
        ]);

        $joke = $jokes->getRandomJoke();

        $this->assertSame('This is a new joke', $joke);

    }

}