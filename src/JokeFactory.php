<?php

namespace HeinHanekom\ChuckNorrisJokes;

class JokeFactory
{

    /**
     * @var array
     */
    protected $jokes = [
        'Joke 1',
        'Joke 2',
        'Joke 3',
        'Joke 4',
    ];

    public function __construct(array $jokes = null)
    {
        if ($jokes){
            $this->jokes = $jokes;
        }
    }

    public function getRandomJoke()
    {
        return $this->jokes[array_rand($this->jokes)];
    }

}